const form = $('#task-form')
const inputDescription = $('#task-input')
const toDoContainer = $('#to-do-container')
const inProgressContainer = $('#in-progress-container')
const doneContainer = $('#done-container')
let prevState = []
let prevInProgress = []
let prevDone = []

const buildTask = (task, index) => {
    return `
            <div class="task" id=${index}>
                <div class="card"">
                    <div class="card-body">
                        <p class="card-text">${task.description}</p>
                        <button data-del=${index} class="btn btn-primary in-progress-btn">In Progress ></button>
                        <button data-del=${index} class="btn btn-primary btnDelete">Delete</button>
                    </div>
                </div>
            </div>
    `
}

const buildTaskInProgress = (task, index) => {
    return `
            <div class="task" id=${index}>
                <div class="card"">
                    <div class="card-body">
                        <p class="card-text">${task.description}</p>
                        <button data-del=${index} class="btn btn-primary done-btn">Done ></button>
                        <button data-del=${index} class="btn btn-primary btnDeleteInProgress">Delete</button>
                    </div>
                </div>
            </div>
    `
}

const buildTaskDone = (task, index) => {
    return `
            <div class="task" id=${index}>
                <div class="card"">
                    <div class="card-body">
                        <p class="card-text">${task.description}</p>
                        <button data-del=${index} class="btn btn-primary btnDeleteDone">Delete</button>
                    </div>
                </div>
            </div>
    `
}
const index = localStorage.getItem('state') ? JSON.parse(localStorage.getItem('state')).length : 0

// Создание новой задачи
form.on('submit', () => {
    const task = {
        index: index,
        description: inputDescription.val(),
    }
    const taskDiv = buildTask(task, index)
    toDoContainer.append(taskDiv)
    localStorage.setItem("data", JSON.stringify(task))

    if (localStorage.getItem('state')) {
        prevState = JSON.parse(localStorage.getItem('state'))
    }
    prevState.push(task)
    localStorage.setItem('state', JSON.stringify(prevState))
})

// Сохранение ввода пользователя в инпуте
if (localStorage.getItem("data")) {
    const data = JSON.parse(localStorage.getItem("data"))
    inputDescription.val(data.description)
}

// Отрисовка всех раннее сохраненных задач при перезагрузке страницы
if (localStorage.getItem('state')) {
    const data = JSON.parse(localStorage.getItem('state'))
    for (let i = 0; i < data.length; i++) {
        const task = buildTask(data[i], i)
        toDoContainer.append(task)
    }
}

// Отрисовка всех раннее сохраненных задач при перезагрузке страницы
if (localStorage.getItem('inProgress')) {
    const data = JSON.parse(localStorage.getItem('inProgress'))
    for (let i = 0; i < data.length; i++) {
        const task = buildTaskInProgress(data[i], i)
        inProgressContainer.append(task)
    }
}

// Отрисовка всех раннее сохраненных задач при перезагрузке страницы
if (localStorage.getItem('done')) {
    const data = JSON.parse(localStorage.getItem('done'))
    for (let i = 0; i < data.length; i++) {
        const task = buildTaskDone(data[i], i)
        doneContainer.append(task)
    }
}


// Удаление задачи из To Do
const btnDelete = $('.btnDelete')
$('body').on('click', )
for (let i = 0; i < btnDelete.length; i++) {
    btnDelete.eq(i).on('click', () => {
        $(`#${btnDelete.eq(i).data('del')}`).remove()
        const store = JSON.parse(localStorage.getItem('state'))
        store.splice(store.findIndex(el => el.index === btnDelete.eq(i).data('del')), 1)
        localStorage.setItem('state', JSON.stringify(store))
    })
}

// Смена задачи из To Do в In Progress
const indexInProgress = localStorage.getItem('state') ? JSON.parse(localStorage.getItem('state')).length : 0
const inProgressArray = []
const inProgressBtn = $('.in-progress-btn')
for (let i = 0; i < inProgressBtn.length; i++) {
    inProgressBtn.eq(i).on('click', () => {
        $(`#${inProgressBtn.eq(i).data('del')}`).remove()
        const store = JSON.parse(localStorage.getItem('state'))
        const taskInProgress = store[store.findIndex(el => el.index === inProgressBtn.eq(i).data('del'))]
        inProgressArray.push(taskInProgress)
        store.splice(store.findIndex(el => el.index === inProgressBtn.eq(i).data('del')), 1)
        localStorage.setItem('state', JSON.stringify(store))
        localStorage.setItem('inProgress', JSON.stringify(inProgressArray))
        const inProgressDiv = buildTaskInProgress(taskInProgress, indexInProgress)
        inProgressContainer.append(inProgressDiv)
        location.reload()
    })
}

// Удаление задачи из In Progress
const btnDeleteInProgress = $('.btnDeleteInProgress')
for (let i = 0; i < btnDeleteInProgress.length; i++) {
    btnDeleteInProgress.eq(i).on('click', () => {
        $(`#${btnDeleteInProgress.eq(i).data('del')}`).remove()
        const store = JSON.parse(localStorage.getItem('inProgress'))
        store.splice(store.findIndex(el => el.index === btnDeleteInProgress.eq(i).data('del')), 1)
        localStorage.setItem('inProgress', JSON.stringify(store))
    })
}

// Смена задачи из In Progress в Done
const indexDone = localStorage.getItem('inProgress') ? JSON.parse(localStorage.getItem('inProgress')).length : 0
const doneArray = []
const doneBtn = $('.done-btn')
for (let i = 0; i < doneBtn.length; i++) {
    doneBtn.eq(i).on('click', () => {
        $(`#${doneBtn.eq(i).data('del')}`).remove()
        const store = JSON.parse(localStorage.getItem('inProgress'))
        const taskDone = store[store.findIndex(el => el.index === doneBtn.eq(i).data('del'))]
        doneArray.push(taskDone)
        store.splice(store.findIndex(el => el.index === doneBtn.eq(i).data('del')), 1)
        localStorage.setItem('inProgress', JSON.stringify(store))
        localStorage.setItem('done', JSON.stringify(doneArray))
        const doneDiv = buildTaskDone(taskDone, indexDone)

        doneContainer.append(doneDiv)

        if (localStorage.getItem('done')) {
            prevDone = JSON.parse(localStorage.getItem('done'))
        }
        location.reload()
    })
}

// Удаление задачи из Done
const btnDeleteDone = $('.btnDeleteDone')
for (let i = 0; i < btnDeleteDone.length; i++) {
    btnDeleteDone.eq(i).on('click', () => {
        $(`#${btnDeleteDone.eq(i).data('del')}`).remove()
        const store = JSON.parse(localStorage.getItem('done'))
        store.splice(store.findIndex(el => el.index === btnDeleteDone.eq(i).data('del')), 1)
        localStorage.setItem('done', JSON.stringify(store))
    })
}










